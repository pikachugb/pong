﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pong
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        public int viteza_stanga = 5;
        public int viteza_sus = 5;
        public int puncte = 0;

        public Form1()
        {
            InitializeComponent();
            Cursor.Hide();
            timer1.Enabled = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Bounds = Screen.PrimaryScreen.Bounds;
            label2.Left = panel1.Width / 2 - label2.Width / 2;
            label2.Top = panel1.Height / 2 - label2.Height / 2;
            platforma.Top = panel1.Bottom - (panel1.Bottom / 10);
        }
        
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();   
            } 

            if (e.KeyCode == Keys.F1)
            {
                minge.Left = 300;
                minge.Top = 300;
                viteza_stanga = 5;
                viteza_sus = 5;
                puncte = 0;
                label1.Text = "PUNCTE: 0";
                label2.Visible = false;
                panel1.BackColor = Color.White;
                timer1.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            platforma.Left = Cursor.Position.X - (platforma.Width / 2);
            minge.Left += viteza_stanga;
            minge.Top += viteza_sus;

            if (minge.Bottom >= platforma.Top && minge.Bottom <= platforma.Bottom && minge.Left >= platforma.Left && minge.Right <= platforma.Right)
            {
                viteza_sus += 2;
                viteza_stanga += 2;
                viteza_sus = -viteza_sus;
                puncte++;
                label1.Text = "PUNCTE: " + puncte.ToString();
                panel1.BackColor = Color.FromArgb(r.Next(200,255),r.Next(200,255),r.Next(200,255));
            }

            if (minge.Left <= panel1.Left)
                viteza_stanga = -viteza_stanga;

            if (minge.Right >= panel1.Right)
                viteza_stanga = -viteza_stanga;

            if (minge.Top<= panel1.Top)
                viteza_sus = -viteza_sus;

            if (minge.Bottom >= panel1.Bottom)
            {
                timer1.Enabled = false;
                label2.Visible = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(0, 0, minge.Width - 3, minge.Height - 3);
            Region rg = new Region(gp);
            minge.Region = rg;
        }
    }
}
